package Tetris;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import Tetris.Piece.I;
import Tetris.Piece.J;
import Tetris.Piece.L;
import Tetris.Piece.Piece;
import Tetris.Piece.O;
import Tetris.Piece.S;
import Tetris.Piece.T;
import Tetris.Piece.Z;

public class PieceManager {
    private static final Random random = new Random();
    private static final List<Piece> pieces = Collections.unmodifiableList(
            new ArrayList<Piece>() {{
                add(new I());
                add(new J());
                add(new L());
                add(new O());
                add(new S());
                add(new T());
                add(new Z());
            }}
    );
    public Piece nextPiece(){
        int index = random.nextInt(7);
        return pieces.get(index);
    }
}
