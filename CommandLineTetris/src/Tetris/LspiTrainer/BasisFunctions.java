package Tetris.LspiTrainer;

import Tetris.BoardState;
import Tetris.Piece.PermissibleMove;
import Tetris.Piece.Piece;
import Tetris.State;
import org.apache.commons.math3.linear.ArrayRealVector;

public class BasisFunctions {
    public static final int NUM_BASIS = 7 -1;
    public static final int INDEX_LANDING_HEIGHT = 0;
    public static final int INDEX_ERODED_PIECES = 1;
    public static final int INDEX_ROW_TRANSITIONS = 2;
    public static final int INDEX_COLUMN_TRANSITIONS = 3;
    public static final int INDEX_EMPTY_HOLES = 4;
    public static final int INDEX_CUMULATIVE_WELLS = 5;
    public static final int INDEX_HOLE_DEPTH = 6;
    static double[] basisFunctionValues = new double[NUM_BASIS];
    public static ArrayRealVector compute(State state, PermissibleMove m){
        BoardState currBoardState = state.getBoardState();
        Piece currPiece = state.getPiece();
        PermissibleMove currMove = m;
        basisFunctionValues[INDEX_LANDING_HEIGHT] = currBoardState.getLowestLandingHeight(currPiece, currMove);
        //basisFunctionValues[INDEX_ERODED_PIECES] = currBoardState.getPieceEroded(currPiece, currMove); //faulty
        basisFunctionValues[INDEX_ROW_TRANSITIONS-1] = currBoardState.getRowTransition(currPiece, currMove);
        basisFunctionValues[INDEX_COLUMN_TRANSITIONS-1] = currBoardState.getColumnTransition(currPiece, currMove);
        basisFunctionValues[INDEX_EMPTY_HOLES-1] = currBoardState.getEmptyHoles(currPiece, currMove);
        basisFunctionValues[INDEX_CUMULATIVE_WELLS-1] = currBoardState.getCumulativeWells(currPiece, currMove);
        basisFunctionValues[INDEX_HOLE_DEPTH-1] = currBoardState.getHoleDepth(currPiece, currMove);
        return new ArrayRealVector(basisFunctionValues);
    }
}
