package Tetris.LspiTrainer;

import static Tetris.LspiTrainer.BasisFunctions.NUM_BASIS;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Tetris.BoardState;
import Tetris.Piece.PermissibleMove;
import Tetris.Piece.Piece;
import Tetris.State;
import Tetris.Visualiser;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.SparseRealMatrix;

public class Trainer {
    public WeightVector weight;
    List<TrainingTuple> tupleList;
    Array2DRowRealMatrix theta;
    public static double DISCOUNT = 1.0;
    public static double EPSILON = 0.0000001;
    int size;
    public Trainer(int trainingTupleSize){
        weight = new WeightVector();
        weight.setWeight(new double[]{-0.009709879630503168, -0.006373570193060138, -0.0036949809141187777, -0.05703966022238628, -0.06821230827763844, -0.005631999572405076});
        size = trainingTupleSize;
        tupleList = new ArrayList<>();
        theta = new Array2DRowRealMatrix(size, NUM_BASIS);
    }
    public void train(){
        // 1. Obtain training tuples
        tupleList = TrainingTupleGenerator.generateTrainingTuples(size);
        getInitTheta();
        //2. Weight randomly generated during initialisation
        //loop until convergence
        //3. Policy improvement
        boolean convergent = false;
        for(int i = 0; i<1000; i++){
            Array2DRowRealMatrix m = estimateThetaLPiTheta(tupleList);
            RealMatrix newWeight = MatrixUtils.inverse(theta.transpose().multiply(theta.subtract(estimateThetaLPiTheta(tupleList).scalarMultiply(DISCOUNT)))).multiply(theta.transpose()).multiply(getRewardMatrix());
            System.out.println(Arrays.toString(newWeight.getColumn(0)));
            weight.setWeight(newWeight.getColumn(0));
        }

    }

    private RealMatrix getRewardMatrix() {
        RealMatrix reward = new Array2DRowRealMatrix(size,1);
        for(int i = 0; i<size; i++){
            reward.setEntry(i,0, tupleList.get(i).reward);
        }
        return reward;
    }

    public PermissibleMove pickMove(State s, WeightVector w) {
        double score = -100000000;
        int optimalMove = -1;
        BoardState b = s.getBoardState();
        Piece p = s.getPiece();
        for(int i = 0; i<p.permissibleMoves.size(); i++){
            double[] currScore = BasisFunctions.compute(s, p.permissibleMoves.get(i)).getDataRef();
            double[] weight = w.getWeight().getDataRef();
            double temp = 0;
            for(int j = 0; j<currScore.length; j++){
                temp+=currScore[j]*weight[j];
            }
            if(temp>score){
                score = temp;
                optimalMove = i;
            }
        }
        return p.permissibleMoves.get(optimalMove);
    }

    public Array2DRowRealMatrix estimateThetaLPiTheta(List<TrainingTuple> tupleList){
        Array2DRowRealMatrix estimate = new Array2DRowRealMatrix(tupleList.size(), BasisFunctions.NUM_BASIS);
        int i = 0;
        for(TrainingTuple tt: tupleList){
            PermissibleMove optimalMove = pickMove(tt.nextState, weight);
            ArrayRealVector currRow = BasisFunctions.compute(tt.nextState, optimalMove);
            estimate.setRow(i, currRow.getDataRef());
            i++;
        }
        return estimate;
    }
    public void getInitTheta(){
        for(int i = 0; i<tupleList.size(); i++){
            TrainingTuple currTuple = tupleList.get(i);
            theta.setRow(i, BasisFunctions.compute(currTuple.currentState, currTuple.currentMove).getDataRef());
        }
    }

    public WeightVector getTrainedWeight(){
        return weight;
    }
    public static void main(String[] args){
        Trainer t = new Trainer(5000);
        t.train();
    }
}
