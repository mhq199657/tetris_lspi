package Tetris.LspiTrainer;
import static Tetris.LspiTrainer.BasisFunctions.INDEX_COLUMN_TRANSITIONS;
import static Tetris.LspiTrainer.BasisFunctions.INDEX_CUMULATIVE_WELLS;
import static Tetris.LspiTrainer.BasisFunctions.INDEX_EMPTY_HOLES;
import static Tetris.LspiTrainer.BasisFunctions.INDEX_ERODED_PIECES;
import static Tetris.LspiTrainer.BasisFunctions.INDEX_HOLE_DEPTH;
import static Tetris.LspiTrainer.BasisFunctions.INDEX_LANDING_HEIGHT;
import static Tetris.LspiTrainer.BasisFunctions.INDEX_ROW_TRANSITIONS;
import static Tetris.LspiTrainer.BasisFunctions.NUM_BASIS;

import java.util.Random;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
public class WeightVector {
    public ArrayRealVector weight;
    public WeightVector(){
        this.weight = new ArrayRealVector(new double[NUM_BASIS]);
    }
    public void randomise(){
        Random random = new Random();
        weight.setEntry(INDEX_LANDING_HEIGHT, random.nextInt(100)-50);
        //weight.setEntry(INDEX_ERODED_PIECES, -random.nextDouble());
        weight.setEntry(INDEX_ROW_TRANSITIONS-1, random.nextInt(100)-50);
        weight.setEntry(INDEX_COLUMN_TRANSITIONS-1, random.nextInt(100)-50);
        weight.setEntry(INDEX_EMPTY_HOLES-1, random.nextInt(100)-50);
        weight.setEntry(INDEX_CUMULATIVE_WELLS-1, random.nextInt(100)-50);
        weight.setEntry(INDEX_HOLE_DEPTH-1, random.nextInt(100)-50);
    }

    public ArrayRealVector getWeight() {
        return weight;
    }
    public void setWeight(double[] newWeight){
        weight = new ArrayRealVector(newWeight);
    }
}
