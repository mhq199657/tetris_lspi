package Tetris.LspiTrainer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Tetris.BoardState;
import Tetris.Piece.PermissibleMove;
import Tetris.Piece.Piece;
import Tetris.PieceManager;
import Tetris.Robot;
import Tetris.State;

public class TrainingTupleGenerator {
    public static PieceManager pieceGenerator = new PieceManager();
    public static Robot movePicker = new Robot();
    public static TrainingTuple generate(){
        Random random = new Random();
        double density = random.nextDouble();
        BoardState randomBoardState = BoardState.randomBoardState(density);
        Piece randomPiece = pieceGenerator.nextPiece();
        State randomState = new State(randomBoardState, randomPiece);
        PermissibleMove randomMove = movePicker.randomMove(randomState);
        BoardState nextBoardState = new BoardState(randomBoardState);
        nextBoardState.applyPiece(randomPiece, randomMove);
        State nextState = new State(nextBoardState, pieceGenerator.nextPiece());
        double reward = Math.pow(nextBoardState.rowsCleared - randomBoardState.rowsCleared,2);
        return new TrainingTuple(randomState, randomMove, nextState, reward);
    }
    public static List<TrainingTuple> generateTrainingTuples(int size) {
        List<TrainingTuple> list = new ArrayList<>();
        for(int i = 0; i<size; i++){
            list.add(generate());
        }
        return list;
    }
}
