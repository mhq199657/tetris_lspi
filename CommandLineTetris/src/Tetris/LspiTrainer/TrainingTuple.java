package Tetris.LspiTrainer;

import Tetris.Piece.PermissibleMove;
import Tetris.State;

public class TrainingTuple {
    public State currentState;
    public PermissibleMove currentMove;
    public State nextState;
    public double reward;
    TrainingTuple(State currentState, PermissibleMove currentMove, State nextState, double reward){
        this.currentState = currentState;
        this.currentMove = currentMove;
        this.nextState = nextState;
        this.reward = reward;
    }
}
