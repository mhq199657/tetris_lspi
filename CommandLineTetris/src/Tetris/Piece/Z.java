package Tetris.Piece;

import static Tetris.Piece.PermissibleMove.ORIENT0;
import static Tetris.Piece.PermissibleMove.ORIENT90;
import static Tetris.BoardState.NUM_COL;

import java.util.ArrayList;

public class Z extends Piece {
    public Z(){
        numOfPermissibleOrientations = 2;
        int currColumn = 0;
        permissibleMoves = new ArrayList<>();
        while(currColumn + 3 <= NUM_COL){ //+1 to one-based and +3 to account for width
            PermissibleMove pm = new PermissibleMove(currColumn, ORIENT0); //Orientation only up
            // **
            //  **
            permissibleMoves.add(pm);
            currColumn++;
        }
        currColumn = 0;
        while(currColumn + 2 <=NUM_COL) {
            PermissibleMove pm = new PermissibleMove(currColumn, ORIENT90);
            permissibleMoves.add(pm);
            currColumn++;
            /*
              *
             **
             *
             */
        }

        permissibleConfigurations = new ArrayList<>();
        permissibleConfigurations.add(new Configuration(new int[][]{{0,1}, {1,1}, {1}}));
        permissibleConfigurations.add(new Configuration(new int[][]{{1,1},{0,1,1}}));
    }
    public String toString(){
        return "Z";
    }
}
