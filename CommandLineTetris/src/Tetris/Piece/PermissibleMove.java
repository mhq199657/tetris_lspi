package Tetris.Piece;

public class PermissibleMove {
    //a permissible move is specified by the column of Tetris.Piece's leftmost block, and its orientation
    public static int ORIENT0= 0;
    public static int ORIENT90 = 1;
    public static int ORIENT180 = 2;
    public static int ORIENT270 = 3;//These degrees are clockwise rotation
    private int column;
    private int orientation;
    public PermissibleMove(int column, int orientation) {
        this.column = column;
        this.orientation = orientation;
    }
    @Override
    public String toString(){
        return "["+column+", "+orientation+"]";
    }
    public int getColumn(){
        return column;
    }
    public int getOrientation(){
        return orientation;
    }
}
