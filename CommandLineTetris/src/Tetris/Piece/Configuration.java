package Tetris.Piece;

public class Configuration {
    //column major
    int[][] configuration;
    public Configuration(int[][] configuration){
        this.configuration = configuration;
    }
    public int[][] getConfiguration(){
        return configuration;
    }
    public int numBlocksOnRow(int row){ //row is 0 based
        int ret = 0;
        for(int i = 0; i<configuration.length; i++){
            int[] currColumn = configuration[i];
            int height = currColumn.length;
            if(row>=height){
                continue;
            }
            ret += currColumn[row];
        }
        return ret;
    }
}
