package Tetris.Piece;

import java.util.ArrayList;

public abstract class Piece {

    public ArrayList<Tetris.Piece.PermissibleMove> permissibleMoves;
    public int numOfPermissibleOrientations;
    public ArrayList<Configuration> permissibleConfigurations; //First index by orientation, next 2d array specify format
    //This is by column, from bottom to up, then row, from left to right. This is to facilitate easy update
}
