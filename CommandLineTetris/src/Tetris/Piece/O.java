package Tetris.Piece;
import static Tetris.Piece.PermissibleMove.ORIENT0;
import static Tetris.BoardState.NUM_COL;

import java.util.ArrayList;

public class O extends Piece {
    public O(){
        numOfPermissibleOrientations = 1;
        int currColumn = 0;
        permissibleMoves = new ArrayList<>();
        while(currColumn + 2 <= NUM_COL){ //+1 to one-based and +1 to account for width
            PermissibleMove pm = new PermissibleMove(currColumn, ORIENT0); //Orientation only up
            permissibleMoves.add(pm);
            currColumn++;
        }
        permissibleConfigurations = new ArrayList<>();
        permissibleConfigurations.add(new Configuration(new int[][]{{1,1},{1,1}}));
    }
    public String toString(){
        return "O";
    }
}
