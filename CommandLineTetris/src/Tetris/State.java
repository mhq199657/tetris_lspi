package Tetris;

import Tetris.Piece.Piece;

public class State {
    private BoardState boardState;
    private Piece piece;
    public State(BoardState boardState, Piece piece){
        this.boardState = boardState;
        this.piece = piece;
    }

    public BoardState getBoardState() {
        return boardState;
    }

    public Piece getPiece() {
        return piece;
    }
}
