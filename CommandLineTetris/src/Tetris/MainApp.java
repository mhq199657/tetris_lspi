package Tetris;

import Tetris.Piece.PermissibleMove;
import Tetris.Piece.Piece;

public class MainApp {
    public static void main(String[] args){
        BoardState s = new BoardState();
        Visualiser v = new Visualiser(s, false);
        Robot r = new Robot();
        PieceManager pm = new PieceManager();
        long startTime = System.nanoTime();
        while(s.isNotDead()){
            Piece p = pm.nextPiece();
            PermissibleMove move = r.generateMove(new State(s, p));
            s.applyPiece(p, move);
            v.visualise();
        }
        long endTime = System.nanoTime();
        System.out.println(endTime-startTime);
    }
}
