package Tetris;

import java.util.Arrays;

public class Visualiser {
    BoardState state;
    boolean isHeadlessMode;
    public Visualiser(BoardState state, boolean isHeadlessMode){
        this.state = state;
        this.isHeadlessMode = isHeadlessMode;
    }
    public void visualise(){
        if(isHeadlessMode){
            return;
        }
        int[][] currBoard = state.columnMajorBoard;
        for(int i = 0; i<currBoard.length; i++){
            System.out.println(Arrays.toString(currBoard[i]));
        }
        System.out.println("Rows cleared: " + state.rowsCleared);
        System.out.println("========================================================");
    }
}
