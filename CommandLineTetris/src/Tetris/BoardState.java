package Tetris;

import java.util.Random;

import Tetris.Piece.Configuration;
import Tetris.Piece.I;
import Tetris.Piece.L;
import Tetris.Piece.O;
import Tetris.Piece.PermissibleMove;
import Tetris.Piece.Piece;

public class BoardState {
    public static final int NUM_COL = 10;
    public static final int NUM_ROW = 20;
    public boolean isDead;
    public int[][] columnMajorBoard;
    public int rowsCleared;
    BoardState(){
        columnMajorBoard = new int[NUM_COL][NUM_ROW];
        isDead = false;
        rowsCleared = 0;
    }
    public BoardState(int[][] givenColumnMajorBoard){
        columnMajorBoard = givenColumnMajorBoard;
        isDead = false;
        rowsCleared = 0;
    }
    public BoardState(BoardState toBeCopied) {
        this.columnMajorBoard= new int[toBeCopied.columnMajorBoard.length][]; // you can access
        for(int i = 0; i<toBeCopied.columnMajorBoard.length; i++){
            this.columnMajorBoard[i] = toBeCopied.columnMajorBoard[i].clone();
        }
        this.isDead = toBeCopied.isDead;
        this.rowsCleared = toBeCopied.rowsCleared;
    }
    public boolean[] applyPiece(Piece p, PermissibleMove m){
    //If indexOutOfBound ->Dead
        int orientation = m.getOrientation();
        int leftMostColumn = m.getColumn();
        Configuration c = p.permissibleConfigurations.get(orientation);
        int[][] piece2DArray = c.getConfiguration();
        int lowestRow = getLowestRow(piece2DArray, leftMostColumn);
        if(lowestRow>=NUM_ROW){
            isDead = true;
        }else{
            try{
                for(int i = 0; i<piece2DArray.length; i++){
                    int[] currPieceColumn = piece2DArray[i];
                    for(int j = 0; j<currPieceColumn.length; j++ ){
                        if(currPieceColumn[j]==0){
                            continue;
                        }
                        assert(columnMajorBoard[i][j+lowestRow]==0);
                        columnMajorBoard[i+leftMostColumn][j+lowestRow] = 1;
                    }
                }
            }catch(IndexOutOfBoundsException iobe){
                isDead = true;
            }
        }
        return clearFullRow();
    }

    public void reset(BoardState backUp) {
        this.rowsCleared = backUp.rowsCleared;
        this.isDead = backUp.isDead;
        this.columnMajorBoard = backUp.columnMajorBoard;
    }

    public int getLowestLandingHeight(Piece p, PermissibleMove m){
        BoardState backUp = new BoardState(this);
        int orientation = m.getOrientation();
        int leftMostColumn = m.getColumn();
        Configuration c = p.permissibleConfigurations.get(orientation);
        int[][] piece2DArray = c.getConfiguration();
        int ret = getLowestRow(piece2DArray, leftMostColumn);
        this.reset(backUp);
        return ret;
    }
    public int getPieceEroded(Piece p, PermissibleMove m){
        BoardState backUp = new BoardState(this);
        int rowsClearedStart = this.rowsCleared;
        boolean[] isRowCleared = applyPiece(p, m);
        int rowsClearedEnd = this.rowsCleared;
        int lowestLandingHeight = getLowestLandingHeight(p,m);
        int numRowCleared = rowsClearedEnd-rowsClearedStart;
        Configuration c = p.permissibleConfigurations.get(m.getOrientation());
        int numSquareContributed = 0;
        for(int i = lowestLandingHeight; i<Math.max(lowestLandingHeight+4, NUM_ROW); i++){
            if(i==NUM_ROW){
                break;
            }
            if(isRowCleared[i]){
                numSquareContributed+=c.numBlocksOnRow(i-lowestLandingHeight);
            }
        }
        this.reset(backUp);
        return numRowCleared * numSquareContributed;
    }

    private boolean[] clearFullRow() {
        boolean[] flag = new boolean[NUM_ROW];
        for(int i = 0; i<NUM_ROW; i++){
            boolean isFull = true;
            for(int j = 0; j<NUM_COL; j++){
                if(columnMajorBoard[j][i]==0){
                    isFull = false;
                    break;
                }
            }
            if(isFull){
                flag[i] = true;
            }
        }
        int oldRow = 0;
        int newRow = 0;
        while(newRow <NUM_ROW){
            if(oldRow==NUM_ROW){
                break;
            }
            if(flag[oldRow]){
                oldRow++;
                rowsCleared++;
            }else{
                if(oldRow==newRow){
                    oldRow++;
                    newRow++;
                    continue;
                }
                for(int i = 0; i<NUM_COL; i++){
                    columnMajorBoard[i][newRow] = columnMajorBoard[i][oldRow];
                }
                oldRow++;
                newRow++;
            }
        }
        for(int i = newRow; i<NUM_ROW; i++ ){
            for(int j = 0; j<NUM_COL; j++){
                columnMajorBoard[j][i] = 0;
            }
        }
        return flag;
    }

    private int getLowestRow(int[][] piece2DArray, int leftMostColumn) {
        int numColumnsAffected = piece2DArray.length;
        int currAtLeast = -1;
        for(int i = 0; i<numColumnsAffected; i++){
            int base = lowestZeroAtColumn(i+leftMostColumn);
            if(base>=NUM_ROW){
                return NUM_ROW; //game is dead already
            }
            int[] currPieceColumn = piece2DArray[i];
            for(int j = 0; j<currPieceColumn.length; j++){
                if(currPieceColumn[j]==0){
                    base--;
                    if(base<0){
                        base = 0;
                    }
                }else{
                    break;
                }
            }
            if(base>currAtLeast){
                currAtLeast = base;
            }
        }
        return currAtLeast;
    }

    private int lowestZeroAtColumn(int column) {
        int[] currColumn = columnMajorBoard[column];
        for(int i = currColumn.length-1; i>=0; i--){
            if(currColumn[i]!=0){
                return i+1;
            }
        }
        return 0;
    }

    public boolean isNotDead(){
        return !isDead;
    }

    public static BoardState randomBoardState(double probability) {
        //return a boardState with maximum height uniformly distributed between 0 to 20
        //Below which it contains value 0's and 1's with probability
        //of getting 1's to be RAND_VALUE 2
        //Therefore, the distribution is linear to both
        Random random = new Random();
        int maxmimumHeight = random.nextInt(NUM_ROW); //0-19
        int[][] randomColumnMajorBoard = new int[NUM_COL][NUM_ROW];
        for(int i = 0; i<maxmimumHeight; i++){
            int count = 0;
            for(int j = 0; j<NUM_COL; j++){
                randomColumnMajorBoard[j][i] = random.nextDouble()>probability? 0:1;
                if(randomColumnMajorBoard[j][i]==1){
                    count++;
                }
            }
            if(count == NUM_COL){
                randomColumnMajorBoard[random.nextInt(NUM_COL)][i] = 0;
            }
        }
        return new BoardState(randomColumnMajorBoard);
    }

    public int getRowTransition(Piece currPiece, PermissibleMove currMove) {
        BoardState backUp = new BoardState(this);
        this.applyPiece(currPiece, currMove);
        int ret =  countRowTransition();
        reset(backUp);
        return ret;
    }

    private int countRowTransition() {
        int count = 0;
        for(int i = 0; i< NUM_ROW; i++) {
            boolean isBlankBefore = false;
            boolean isBlankAfter = columnMajorBoard[1][i] == 0;
            for(int j = 0; j < NUM_COL; j++) {
                if(columnMajorBoard[j][i]==1 &&( isBlankBefore || isBlankAfter)){
                    count++;
                }
                isBlankBefore = columnMajorBoard[j][i]==0;
                if (j>=NUM_COL - 2){
                    isBlankAfter = false;
                }else{
                    isBlankAfter = columnMajorBoard[j+2][i]==0;
                }
            }
        }
        return count;
    }

    public int getColumnTransition(Piece currPiece, PermissibleMove currMove) {
        BoardState backUp = new BoardState(this);
        this.applyPiece(currPiece, currMove);
        int ret =  countColumnTransition();
        reset(backUp);
        return ret;
    }

    private int countColumnTransition() {
        int count = 0;
        for(int i = 0; i<NUM_COL; i++) {
            boolean isBlankBefore = false;
            boolean isBlankAfter = columnMajorBoard[i][1]==0;
            for(int j = 0; j<NUM_ROW; j++) {
                if(columnMajorBoard[i][j]==1 &&(isBlankBefore||isBlankAfter)){
                    count++;
                }
                isBlankBefore = columnMajorBoard[i][j] == 0;
                if(j >= NUM_ROW -2) {
                    isBlankAfter = false;
                }else{
                    isBlankAfter = columnMajorBoard[i][j+2] == 0;
                }
            }
        }
        return count;
    }

    public int getEmptyHoles(Piece currPiece, PermissibleMove currMove) {
        BoardState backUp = new BoardState(this);
        this.applyPiece(currPiece, currMove);
        int ret =  countEmptyHoles();
        reset(backUp);
        return ret;
    }

    private int countEmptyHoles() {
        int count = 0;
        for(int i = 0; i<NUM_COL; i++) {
            int currColumnCount = 0;
            int temp = 0;
            for(int j = 0; j<NUM_ROW; j++) {
                if(columnMajorBoard[i][j]==0){
                    temp++;
                }else{
                    currColumnCount +=temp;
                    temp = 0;
                }
            }
            count+=currColumnCount;
        }
        return count;
    }

    public int getCumulativeWells(Piece currPiece, PermissibleMove currMove) {
        BoardState backUp = new BoardState(this);
        this.applyPiece(currPiece, currMove);
        int ret =  countCumulativeWells();
        reset(backUp);
        return ret;
    }

    private int countCumulativeWells() {
        int count = 0;
        int heightBefore = NUM_ROW;
        int heightAfter = lowestZeroAtColumn(1);
        for(int i = 0; i<NUM_COL; i++) {
            int borderHeight = Math.min(heightBefore, heightAfter);
            int currColumnHeight = lowestZeroAtColumn(i);
            count +=Math.max(0, borderHeight - currColumnHeight);
            heightBefore = currColumnHeight;
            if(i>=NUM_COL-2){
                heightAfter = 20;
            }else{
                heightAfter = lowestZeroAtColumn(i+2);
            }
        }
        return count;
    }

    public int getHoleDepth(Piece currPiece, PermissibleMove currMove) {
        BoardState backUp = new BoardState(this);
        this.applyPiece(currPiece, currMove);
        int ret =  countHoleDepth();
        reset(backUp);
        return ret;
    }

    private int countHoleDepth() {
        int count = 0;
        for(int i = 0; i<NUM_COL; i++) {
            int aboveHeight = lowestZeroAtColumn(i);
            int belowHeight = NUM_ROW;
            int[] currColumn = columnMajorBoard[i];
            for (int j = 0; j < NUM_ROW; j++) {
                if (currColumn[j] == 0) {
                    belowHeight = j;
                    break;
                }
            }
            count += (aboveHeight - belowHeight);
        }
        return count;
    }
    public static void main(String[] args){

        int[][] columnMajorBoard = new int[10][];
        columnMajorBoard[0] = new int[]{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        columnMajorBoard[1] = new int[]{0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        columnMajorBoard[2] = new int[]{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        columnMajorBoard[3] = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        columnMajorBoard[4] = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        columnMajorBoard[5] = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        columnMajorBoard[6] = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        columnMajorBoard[7] = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        columnMajorBoard[8] = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        columnMajorBoard[9] = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        BoardState b = new BoardState(columnMajorBoard);
        Visualiser v = new Visualiser(b, false);
        v.visualise();
        b.getPieceEroded(new O(), new PermissibleMove(0,0));
        v.visualise();
    }
}
