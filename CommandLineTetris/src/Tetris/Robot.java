package Tetris;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import Tetris.LspiTrainer.BasisFunctions;
import Tetris.LspiTrainer.WeightVector;
import Tetris.Piece.PermissibleMove;
import Tetris.Piece.Piece;

public class Robot {
    public static WeightVector w = new WeightVector();
    public Robot(){
        w.setWeight(new double[]{-0.0030612046421137507, -0.007715876150042935, 0.0051594899482262655, -0.06615238840490505, -0.06407973931247576, -0.0030083700943995986});
    }
    public PermissibleMove generateMove(State state) {
        //stub
        return pickMove(state, w);
    }
    public PermissibleMove randomMove(State state){
        BoardState boardState = state.getBoardState();
        Piece piece = state.getPiece();
        Random random = new Random();
        List<PermissibleMove> list = piece.permissibleMoves;
        int size = list.size();
        return list.get(random.nextInt(size));
    }
    public PermissibleMove pickMove(State s, WeightVector w) {
        double score = -100000000;
        int optimalMove = -1;
        BoardState b = s.getBoardState();
        Piece p = s.getPiece();
        for(int i = 0; i<p.permissibleMoves.size(); i++){
            double[] currScore = BasisFunctions.compute(s, p.permissibleMoves.get(i)).getDataRef();
            System.out.println(Arrays.toString(currScore));
            double[] weight = w.getWeight().getDataRef();
            System.out.println(Arrays.toString(weight));
            double temp = 0;
            for(int j = 0; j<currScore.length; j++){
                temp+=currScore[j]*weight[j];
            }
            System.out.println(temp);
            if(temp>score){
                score = temp;
                optimalMove = i;
            }
        }
        return p.permissibleMoves.get(optimalMove);
    }

}
